var imageLinks=['./images/photo1.jpeg','./images/photo2.jpg','./images/photo3.jpeg',
                './images/photo4.jpeg','./images/photo5.jpeg','./images/photo6.jpeg'];
var currentImage=0;
var nextButton=document.getElementById('next');
var previousButton=document.getElementById('previous');
 var imageDiv=document.getElementById('imageHolder');
function showFirstImage(){
    imageDiv.style.backgroundImage="url("+ imageLinks[currentImage]+")";
    imageDiv.style.backgroundRepeat="no-repeat";
    imageDiv.style.backgroundSize="contain";
    console.log('current Image is '+currentImage);
    previousButton.disabled=true;
    previousButton.style.visibility='hidden';
}
function showNextImage(){
    if(currentImage<=4){
    imageDiv.style.backgroundImage="url("+ imageLinks[++currentImage]+")";
    imageDiv.style.backgroundRepeat="no-repeat";
    imageDiv.style.backgroundSize="contain";
    console.log('current Image is '+currentImage);
    }
    if(currentImage==5)
    {
        nextButton.disabled=true;
        nextButton.style.visibility='hidden';
    }
    if(currentImage>0){
        previousButton.disabled=false;
        previousButton.style.visibility='visible';
    }
}

function showPreviousImage(){
    if(currentImage<=5&&currentImage>0){
    imageDiv.style.backgroundImage="url("+ imageLinks[--currentImage]+")";
    imageDiv.style.backgroundRepeat="no-repeat";
    imageDiv.style.backgroundSize="contain";
    console.log('current Image is '+currentImage);
    }
    if(currentImage===0)
    {
        previousButton.disabled=true;
        previousButton.style.visibility='hidden';
    }
    if(currentImage<=4)
    {
        nextButton.disabled=false;
        nextButton.style.visibility='visible';
    }
}